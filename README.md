# DSBDirect
This Android client software can access **DSB boards** which some schools use for substitute plans.

Use it as a replacement for the proprietary DSBmobile app. It loads a lot faster and doesn't make you click any buttons before you can see your plan.

* Displays entries in a nice, native list which you can filter
* Alternatively displays the raw website or image
* View older versions of your plan, stored locally
* Notifications containing your only entries

Sometimes, it stops working, because heinekingmedia, the company operating DSB and its servers, is not happy about it. (They want to sell DSBplan, something that looks somewhat similar to DSBDirect, as a paid extension to their services.)

## Download

Android 4.0 and up. Notification on Android 5 and up. (Nowadays, nobody has Android 4 anymore. Truly.) 

### [Download via release page](https://notabug.org/fynngodau/DSBDirect/releases) | [Download on F-Droid](https://f-droid.org/de/packages/godau.fynn.dsbdirect/) | [Download on Google Play](https://play.google.com/store/apps/details?id=ga.testapp.dsbdirect)

Code mirrors: [notabug.org](https://notabug.org/fynngodau/DSBDirect) | [Codeberg](https://codeberg.org/fynngodau/DSBDirect) | [F-Droid source tarballs](https://f-droid.org/packages/godau.fynn.dsbdirect) | [GitLab](https://gitlab.com/fynngodau/dsbdirectmirror)

## Screenshots

| **Filtered list** ![Filtered list with cards](https://notabug.org/fynngodau/DSBDirect/raw/master/fastlane/metadata/android/de/images/phoneScreenshots/00.png "Filtered list with cards") | **Unfiltered list with dark theme** ![List with dark theme](https://notabug.org/fynngodau/DSBDirect/raw/master/fastlane/metadata/android/de/images/phoneScreenshots/01.png "List with dark theme") |
|:------:|:------:|
| **Website view** ![Website view](https://notabug.org/fynngodau/DSBDirect/raw/master/fastlane/metadata/android/de/images/phoneScreenshots/02.png "Website view") | **Picture view** ![Picture view](https://notabug.org/fynngodau/DSBDirect/raw/master/fastlane/metadata/android/de/images/phoneScreenshots/03.png "Picture view") |


![Boat on water](https://notabug.org/fynngodau/DSBDirect/raw/master/fastlane/metadata/android/en-US/images/featureGraphic.png)
