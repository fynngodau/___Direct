# Datenschutzhinweise | Privacy information

## Deutsch | German

Der Entwickler selbst erhält keinerlei Informationen. DSBDirect ist lediglich ein eigenständig entwickeltes Programm, das nicht mit heinekingmedia in Verbindung steht. Diese Dienste werden beim Gebrauch der App angefragt:

### DSB-Server, für DSB-Zugriff

Möglicherweise werden bei den DSB-Endpoints die gleichen Daten wie auf der Webseite digitales-schwarzes-brett.de gespeichert. Dort speichert heinekingmedia manche Daten, die nach einer Woche "gelöscht oder verfremdet" werden. Schau dir lieber [das ganze Ding](https://digitales-schwarzes-brett.de/datenschutz.html) anstatt nur diesen Ausschnitt an:

>Folgende Daten werden erhoben:
>
> 1.  Informationen über den Browsertyp und die verwendete Version
> 2.  Das Betriebssystem des Nutzers
> 3.  Den Internet-Service-Provider des Nutzers
> 4.  Datum und Uhrzeit des Zugriffs
> 5.  Die IP-Adresse des Nutzers (nur anonymisiert)
> 6.  Websites, von denen das System des Nutzers auf unsere Internetseite gelangt

#### Hinweis

Der Entwickler ist bemüht, kann aber nicht versprechen, dass die obigen Informationen aktuell und akkurat sind.

### notabug.org-Server, für Updatebenachrichtigungen

Notabug.org macht keinen Gebrauch von irgendwelchem besonderen Tracking. Zugriffslogs enthalten IP-Adressen, welche nach höchstens zwei Wochen gehasht werden. [Nochmal mit mehr Sätzen lesen.](https://notabug.org/tos#Privacy)

Wenn du keine Updatebenachrichtigungen wünschst, kannst du sie in den Einstellungen ausschalten. In der F-Droid-Ausgabe ist dies bereits standartmäßig der Fall.

Wenn der Server eine ungültige Antwort ausgibt, wird dir angeboten, nach einer Fehlerbehebung zu suchen. Wenn du das tust, wird ganz ähnlich einer Updatesuche eine Verbindung zu notabug.org aufgebaut.


## Englisch | English

The developer does not receive any information by himself. DSBDirect is simply an independent client that is not related to heinekingmedia. These services are contacted during app use:

### DSB servers, for DSB access

Maybe, the same data as on the website digitales-schwarzes-brett.de is collected at the DSB endpoints. On the website, heinekingmedia stores some data, and it is "deleted or alienated" after a week. Excerpt's translation by me. Better check [the full German thing](https://digitales-schwarzes-brett.de/datenschutz.html).

>The following data is collected:
>
> 1.  Information about browser type and used version
> 2.  The user's operating system
> 3.  The user's ISP
> 4.  Access time and date
> 5.  User's IP-Address (anonymized only)
> 6.  Websites from which the user's system reaches our website

#### Notice

The developer is trying to ensure it, but he can't promise that above information is current and accurate.

### notabug.org servers, for update checks

Notabug.org does not employ any special tracking features. Access logs contain IP addresses, which are hashed after at most 2 weeks. [Read this again but with more sentences.](https://notabug.org/tos#Privacy)

If you do not want to use update checks, you can disable them in the preferences. They are already disabled by default in the F-Droid build.

In case the server returns an invalid response, you are able to "look for a fix". Much like checking for updates, this also contacts notabug.org.
