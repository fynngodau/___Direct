package godau.fynn.dsbdirect.manager;

import android.content.Context;
import godau.fynn.dsbdirect.Utility;
import godau.fynn.dsbdirect.table.Shortcode;

import javax.annotation.Nullable;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

public class ShortcodeManager {

    private Context context;
    private Shortcode[] shortcodes;

    public ShortcodeManager(Context context) {
        this.context = context;
    }

    public Shortcode[] read() {

        Set<String> shortcodeSet = new Utility(context).getSharedPreferences().getStringSet("shortcodes", new LinkedHashSet<String>());
        String[] shortcodeStrings = shortcodeSet.toArray(new String[shortcodeSet.size()]);

        Shortcode[] shortcodes = new Shortcode[shortcodeSet.size()];

        for (int i = 0; i < shortcodeSet.size(); i++) {
            try {
                shortcodes[i] = new Shortcode(shortcodeStrings[i]);
            } catch (IllegalArgumentException e) {
                // Better not be null
                shortcodes[i] = new Shortcode("", "");
                e.printStackTrace();
            }
        }

        this.shortcodes = shortcodes;

        return shortcodes;
    }

    public void write(Shortcode[] shortcodes) {
        this.shortcodes = shortcodes;
        write();

    }

    public void write() {
        String[] shortcodeStrings = new String[shortcodes.length];

        for (int i = 0; i < shortcodes.length; i++) {
            shortcodeStrings[i] = shortcodes[i].serialize();
        }

        new Utility(context).getSharedPreferences()
                .edit()
                .putStringSet("shortcodes", new LinkedHashSet<String>(Arrays.asList(shortcodeStrings)))
                .apply();

    }

    /**
     * @param shortcode The shortcode to remove
     * @return Whether the shortcode was successfully removed
     */
    public boolean removeShortcode(Shortcode shortcode) {

        // Don't remove if not contained
        boolean contained = false;
        for (Shortcode s :
                shortcodes) {
            if (s.equals(shortcode)) contained = true;
        }
        if (!contained) return false;


        // Create new shortcodes array with a size smaller than the old one by 1
        Shortcode[] shortcodes = new Shortcode[this.shortcodes.length - 1];

        int added = 0; // Track the next unused index in new shortcodes array; incremented when accessed

        for (int i = 0; i < this.shortcodes.length; i++) {
            // Don't add the login to be removed again
            if (!this.shortcodes[i].equals(shortcode)) {
                shortcodes[added++] = this.shortcodes[i];
            }
        }

        // Save new logins array
        write(shortcodes);

        return true;
    }

    /**
     * @param shortcode The shortcode to store
     * @return Whether the login was added
     */
    public boolean addShortcode(Shortcode shortcode) {

        // Don't save logins that are lacking either from or to
        if (shortcode.getFrom().isEmpty() || shortcode.getTo().isEmpty()) {
            return false;
        }

        // Create new shortcodes array with a size larger than the old one by 1
        shortcodes = Arrays.copyOf(shortcodes, shortcodes.length + 1);
        // Save new shortcode in new space
        shortcodes[shortcodes.length - 1] = shortcode;

        write();

        return true;
    }

    public String replace(String s) {
        if(!new Utility(context).getSharedPreferences().getBoolean("shortcodes_enabled", false))
            return s;
        if (shortcodes == null) {
            read();
        }
        for (Shortcode shortcode :
                shortcodes) {
            if (shortcode.containedIn(s)) {
                s = shortcode.replace(s);
                break;
            }
        }
        return s;
    }
}
