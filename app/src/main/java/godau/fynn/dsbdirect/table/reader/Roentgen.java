/*
 * DSBDirect
 * Copyright (C) 2019 Fynn Godau
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * This software is not affiliated with heinekingmedia GmbH, the
 * developer of the DSB platform.
 */

package godau.fynn.dsbdirect.table.reader;

import android.content.Context;
import godau.fynn.dsbdirect.table.Entry;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.Date;

public class Roentgen extends Reader {


    public Roentgen(String html, Context context) {
        super(html, context);

    }

    @Override
    public ArrayList<Entry> read() {

        Document document = Jsoup.parse(mHtml);

        // Find date

        String dateString = document.selectFirst("h2").text().replaceAll("Vertretungsplan für \\w+, ", "");
        String[] dateDigits = dateString.split("\\.");
        Date date = new Date(Integer.parseInt(dateDigits[2]) - 1900 /* years start at 1900 */, Integer.parseInt(dateDigits[1]) - 1 /* months start with 0 */,
                Integer.parseInt(dateDigits[0])); // because SimpleDateTime did deliver good results


        // Prepare parsing tables


        Elements h4Elements = document.select("h4");
        String[] h4Strings = new String[h4Elements.size()];

        int substituteTableIndex = 0;

        for (int i = 0; i < h4Elements.size(); i++) {
            h4Strings[i] = h4Elements.get(i).text();

            if (h4Strings[i].equals("Vertretungen:")) {
                substituteTableIndex = i;
            }
        }

        Elements tables = document.getElementsByTag("table");

        // Add substitutes

        Element substituteTable = tables.get(substituteTableIndex);
        Elements rows = substituteTable.select("tr");

        String teacher = "";

        for (Element row :
                rows) {
            ArrayList<String> rowList = new ArrayList<>();

            Elements columns = row.select("td");
            for (Element column :
                    columns) {
                rowList.add(column.text());
            }

            String newTeacher = row.select("th").text();
            if (newTeacher.length() > 0) {
                teacher = newTeacher;
            }

            if (rowList.size() >= 4) {
                addEntry(rowList.get(1).replace("(", " ("), rowList.get(0), teacher, combine(rowList.get(2), rowList.get(3)), date);
            }
        }


        if (tables.size() > h4Strings.length) {
            // Add general announcements

            Element generalTable = tables.get(tables.size() - 1 /* we don't want to be off by one */);

            Elements generalTableRows = generalTable.select("th");

            for (Element row :
                    generalTableRows) {
                addEntry("", "", "", row.text(), date);
            }

        }

        return getEntries();
    }

    @Nullable
    @Override
    public String getSchoolName() {
        return null;
    }

    private String combine(String s, String s1) {
        if (s.length() != 0 && s1.length() != 0) {
            return s + " – " + s1;
        } else if (s.length() == 0 && s1.length() != 0) {
            return s1;
        } else if (s.length() != 0 && s1.length() == 0) {
            return s;
        } else {
            return "";
        }
    }
}
