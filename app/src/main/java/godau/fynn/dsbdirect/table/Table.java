/*
 * DSBDirect
 * Copyright (C) 2019 Fynn Godau
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * This software is not affiliated with heinekingmedia GmbH, the
 * developer of the DSB platform.
 */

package godau.fynn.dsbdirect.table;

import java.io.IOException;
import java.util.Date;

public class Table {
    private String uri;
    private Date publishedTime;
    private int contentType;
    private String title;

    /**
     * Not for actual content, mustn't appear in Table objects as only their children are read out
     * in {@link godau.fynn.dsbdirect.table.reader.Reader}
     */
    private static final int CONTENT_ROOT = 0;
    /**
     * Unknown what this is good for, therefore a mystery
     */
    private static final int CONTENT_MYSTERY = 1;
    /**
     * Not for actual content, holds more tables in its children. Mustn't become a Table object
     */
    private static final int CONTENT_PARENT = 2;
    /**
     * HTML content
     */
    public static final int CONTENT_HTML = 3;
    /**
     * Image content
     */
    public static final int CONTENT_IMAGE = 4;
    /**
     * Text content
     */
    private static final int CONTENT_TEXT = 5;
    /**
     * Also HTML content…? We don't know the difference to {@link #CONTENT_HTML}, so it's a mystery
     */
    public static final int CONTENT_HTML_MYSTERY = 6;

    public Table(String uri, Date publishedTime, int contentType, String title) {
        this.uri = uri;
        this.publishedTime = publishedTime;
        this.contentType = contentType;
        this.title = title;
    }

    public Table(String uri, Date publishedTime, boolean isHtml, String title) {
        this.uri = uri;
        this.publishedTime = publishedTime;
        if (isHtml) {
            contentType = CONTENT_HTML;
        } else {
            contentType = CONTENT_IMAGE;
        }
        this.title = title;
    }

    public Date getPublishedDate() {
        return publishedTime;
    }

    public String getUri() {
        return uri;
    }

    public boolean isHtml() {
        return contentType == CONTENT_HTML || contentType == CONTENT_HTML_MYSTERY;
    }

    /**
     * Don't use this method to verify if a content is HTML, use {@link #isHtml()} for that
     * @return the content type. One of {@link #CONTENT_HTML}, {@link #CONTENT_IMAGE}, or {@link #CONTENT_HTML_MYSTERY}.
     */
    public int getContentType() {
        return contentType;
    }

    public String getTitle() {
        return title;
    }
}
