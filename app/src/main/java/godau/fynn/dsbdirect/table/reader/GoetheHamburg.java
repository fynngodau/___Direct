/*
 * DSBDirect
 * Copyright (C) 2019 Jasper Michalke <jasper.michalke@jasmich.ml>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * This software is not affiliated with heinekingmedia GmbH, the
 * developer of the DSB platform.
 */

package godau.fynn.dsbdirect.table.reader;

import android.content.Context;
import android.util.Log;

import godau.fynn.dsbdirect.activity.MainActivity;
import godau.fynn.dsbdirect.table.Entry;
import godau.fynn.dsbdirect.table.Table;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import javax.annotation.Nullable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class GoetheHamburg extends Reader {


    public GoetheHamburg(String html, Context context) {
        super(html, context);

    }

    @Override
    public ArrayList<Entry> read() {

        Document document = Jsoup.parse(mHtml);

        // Find date

        Date date = null;
        try {
            String dateString = document.selectFirst(".dayHeader, legend").text().replaceAll(", \\w+", "");
            date = new SimpleDateFormat("dd.MM.yyyy").parse(dateString);
        } catch (ParseException e) {
            date=new Date();
            e.printStackTrace();
        }

        //android.widget.Toast.makeText(getApplicationContext(), dateString, Toast.LENGTH_SHORT).show();


        // Prepare parsing tables


        Elements classBlocks = document.select("table");
        String[] classBlocksStrings = new String[classBlocks.size()];

        Elements rows = classBlocks.select("tr");

        String teacher = "";
        String lastClass = ""; // Every class is just listed once even if there are several substitutes

        for (Element row :
                rows) {
            try {

                String infoText;
                String affectedClass;
                try {
                    infoText=row.select("td").get(1).text();
                    affectedClass=row.select("td").get(0).text();
                    lastClass=affectedClass;
                } catch (Exception e) {
                    infoText=row.select("td").get(0).text();
                    Log.d("Multi row", infoText);
                    affectedClass=lastClass;
                }

                String lesson="";
                Pattern pattern = Pattern.compile("(\\d.*) Std");
                Matcher matcher = pattern.matcher(infoText);
                if(matcher.find()) lesson=matcher.group(1).replaceAll("\\.", "");;


                teacher = "";
                pattern = Pattern.compile("bei ([A-ZÄÖÜ]{3}, [A-ZÄÖÜ]{3}|[A-ZÄÖÜ]{3})");
                matcher = pattern.matcher(infoText);
                if(matcher.find()) teacher=matcher.group(1);


                String subject="";
                pattern = Pattern.compile("\\d.* Std. (\\w+)");
                matcher = pattern.matcher(infoText);
                if(matcher.find()&& !matcher.group(1).equals("bei")) subject=matcher.group(1)+" ";


                String info="";
                String[] regex={"im Raum ([HVGFT]\\d{3}|ESH)", "statt bei [A-ZÄÖÜ]{3}", "\\(.*\\)", "(f.llt aus|Vtr. ohne Lehrer)"};
                for (int i = 0; i < regex.length; i++) {
                    pattern = Pattern.compile(regex[i]);
                    matcher = pattern.matcher(infoText);
                    if(matcher.find()) info=matcher.group(0);
                }

                info=subject+info;


                if(!info.contains("im Raum")) {
                    pattern = Pattern.compile("im Raum ([HVGFT]\\d{3}|ESH)");
                    matcher = pattern.matcher(infoText);
                    if(matcher.find()) info+=" ("+matcher.group(1)+")";
                }

                addEntry(affectedClass, lesson, teacher, info, date);
            }
            catch (Exception e) {
                Log.d("GOETHE","Error in row: "+row.text());
                addEntry(null, null, null, row.text(), date);
            }

        }

        return getEntries();
    }

    @Nullable
    @Override
    public String getSchoolName() {
        return null;
    }
}
