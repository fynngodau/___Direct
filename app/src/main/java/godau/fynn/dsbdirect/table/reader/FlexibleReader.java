package godau.fynn.dsbdirect.table.reader;

import android.content.Context;
import godau.fynn.dsbdirect.Utility;
import org.jsoup.select.Elements;

import java.util.Arrays;
import java.util.Date;

/**
 * FlexibleReader is used for implementing the concept of a master table
 * more easily. A (theoretical) master table has columns for every column
 * that could appear in a substitution plan and is then used to construct
 * an entry with the four fields DSBDirect uses. Therefore, columns found
 * in a substitution plan file are assigned to their corresponding master
 * table columns.
 */
public abstract class FlexibleReader extends Reader {
    int UNDEFINED   =  0;
    int CLASS       =  1;
    int SUBJECT     =  2;
    int LESSON      =  3;
    int TYPE        =  4;
    int TEACHER     =  5;
    int ROOM        =  6;
    int OLD_SUBJECT =  7;
    int OLD_TEACHER =  8;
    int OLD_CLASS   =  9;
    int INFO        = 10;
    int OLD_TIME    = 11;
    int MASTER_SIZE = 12;

    public FlexibleReader(String html, Context context) {
        super(html, context);
    }

    /**
     * @return where in the imaginary master table each column belongs
     */
    protected int[] getMasterTablePositions(String[] definitions) {

        int[] positions = new int[definitions.length];

        // Test each definition for matches with strings
        for (int i = 0; i < definitions.length; i++) {
            positions[i] = getMasterTablePosition(definitions[i]);
        }

        return positions;
    }

    /**
     * @return where in the master table the column with this title belongs
     */
    protected abstract int getMasterTablePosition(String definition);

    /**
     * Constructs an entry and then adds it
     */
    protected void constructEntry(String[] masterRow, Date date) {
        String classString = ratherThisThanThat(masterRow[CLASS], masterRow[OLD_CLASS]);
        String subject = ratherThisThanThat(masterRow[SUBJECT], masterRow[OLD_SUBJECT]);

        String combinedClassString = Utility.smartConcatenate(new String[]{classString, subject}, " · ");

        String lesson = masterRow[LESSON];

        String teacher = ratherThisThanThat(masterRow[TEACHER], masterRow[OLD_TEACHER]);

        String info = Utility.smartConcatenate(new String[]{
                masterRow[TYPE], masterRow[OLD_TIME], masterRow[ROOM], masterRow[INFO]
        }, " · ");

        addEntry(combinedClassString, lesson, teacher, info, date);
    }

    /**
     * Prefers to return string1, but returns string2 surrounded by strike tags if string1 is null, empty or just dashes.
     */
    private String ratherThisThanThat(String string1, String string2) {

        final String DASHES_REGEX = "-+(?!.)"; // matches one or more "-" if nothing else follows it

        if (string1 != null && !string1.isEmpty() && !string1.matches(DASHES_REGEX)) {
            return string1;
        } else if (string2 != null) { // Don't concatenate around with null

            return "<strike>" + string2 + "</strike>";
        } else {
            return null;
        }
    }

    protected String[] stringsOfElements(Elements e) {
        Object[] mainTableDefinitionObjects = e.eachText().toArray();
        return Arrays.copyOf(mainTableDefinitionObjects,
                mainTableDefinitionObjects.length, String[].class);
    }
}
