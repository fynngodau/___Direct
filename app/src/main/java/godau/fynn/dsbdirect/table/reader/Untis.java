/*
 * DSBDirect
 * Copyright (C) 2019 Fynn Godau
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * This software is not affiliated with heinekingmedia GmbH, the
 * developer of the DSB platform.
 */

package godau.fynn.dsbdirect.table.reader;

import android.content.Context;
import android.util.Log;
import godau.fynn.dsbdirect.Utility;
import godau.fynn.dsbdirect.table.Entry;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class Untis extends FlexibleReader {


    public Untis(String html, Context context) {
        super(html, context);
    }

    private Document d;

    @Override
    public ArrayList<Entry> read() {

        // Leave brs be
        mHtml = mHtml.replaceAll("<br>", "&lt;br&gt;");

        // Leave strikes be
        mHtml = mHtml.replaceAll("<s(trike)*>", "&lt;strike&gt;");
        mHtml = mHtml.replaceAll("</s(trike)*>", "&lt;&#47;strike&gt;");

        if (d == null) {
            d = Jsoup.parse(mHtml);
        }

        // Tables are inside center tags
        Elements centers = d.getElementsByTag("center");


        // Every other center contains an advertisement for Untis
        for (int centerIndex = 0; centerIndex < centers.size(); centerIndex += 2) {

            Element center = centers.get(centerIndex);

            // Get which date this center is about
            String dateString = center.selectFirst("div").text();
            String[] dateDigits = dateString.split(" ")[0].split("\\.");

            Calendar calendar = Utility.zeroOClock(Calendar.getInstance());
            calendar.set(Calendar.DAY_OF_MONTH, Integer.parseInt(dateDigits[0]));
            calendar.set(Calendar.MONTH, Integer.parseInt(dateDigits[1]) - 1);
            calendar.set(Calendar.YEAR, Integer.parseInt(dateDigits[2]));
            Date date = calendar.getTime();

            // Get info box, if present
            Elements infoTables = center.getElementsByClass("info");
            if (infoTables.size() != 0) {
                Element infoTableBody = infoTables.first().getElementsByTag("tbody").first();

                Elements infoTableTrs = infoTableBody.getElementsByTag("tr");

                /* First tr will (probably) contain "Nachrichten zum Tag" headline, but we check it anyway because
                 * the headline is inside a th tag, not a td tag.
                 */
                for (Element tr : infoTableTrs) {

                    Elements tds = tr.getElementsByTag("td");


                    if (tds.size() > 0 && !isUselessLine(tds.first().text())) {
                        // Construct an entry for this line

                        // If there are two columns: separate them with a ':'
                        addEntry(null, null, null,
                                Utility.smartConcatenate(tds, ": "),
                                date
                        );
                    }


                }

            }

            // Get main table
            Elements mainTables = center.getElementsByClass("mon_list"); // There should be exactly one
            if (mainTables.size() > 0) {
                Element mainTableBody = mainTables.first().getElementsByTag("tbody").first();

                if (mainTableBody == null) {
                    // No actual table yet on this page?
                    continue;
                }

                Elements mainTableTrs = mainTableBody.getElementsByTag("tr");



                // Get definitions from the first row
                String[] mainTableDefinitions = stringsOfElements(
                        mainTableTrs.first().getElementsByTag("th")
                );
                int[] positions = getMasterTablePositions(mainTableDefinitions);

                String inlineClass = null;

                // Get every row
                // Start with 1 because first row contained definitions
                row:
                for (int trIndex = 1; trIndex < mainTableTrs.size(); trIndex++) {
                    Element tr = mainTableTrs.get(trIndex);

                    Elements tds = tr.getElementsByTag("td");
                    String[] masterRow = new String[MASTER_SIZE];

                    // Get value from every column
                    for (int tdIndex = 0; tdIndex < tds.size(); tdIndex++) {
                        Element td = tds.get(tdIndex);



                        String s = td.text();

                        if (td.hasClass("inline_header")) {
                            // This is an inline class definition
                            inlineClass = s;
                            continue row;
                        } else {

                            if (s.matches("<s(trike)*>.+</s(trike)*>\\?.*")) {
                                // See https://notabug.org/fynngodau/DSBDirect/issues/58
                                s = s.split("\\?")[1];
                            }

                            masterRow[positions[tdIndex]] = s;
                        }
                    }

                    // Add inline class if class not provided by entry
                    if (masterRow[CLASS] == null && inlineClass != null) {
                        masterRow[CLASS] = inlineClass;
                    }

                    constructEntry(masterRow, date);

                }

            }


        }

        Log.d("UNTISREAD", "Read out " + getEntries().size() + " entries");
        return getEntries();

    }

    public String getSchoolName() {

        if (d == null) {
            d = Jsoup.parse(mHtml);
        }


        Element monHead = d.getElementsByClass("mon_head").first();
        Element p = monHead.getElementsByTag("tbody").first()
                .getElementsByTag("tr").first()
                .getElementsByAttributeValue("align", "right").first()
                .getElementsByTag("p").first();

        return p.textNodes().get(0).text();
    }

    protected int getMasterTablePosition(String definition) {
        switch (definition) {
            case "Klasse(n)":
                return CLASS;
            case "Fach":
                return SUBJECT;
            case "Stunde":
            case "Std.":
                return LESSON;
            case "Art":
                return TYPE;
            case "Vertreter":
                return TEACHER;
            case "Raum":
                return ROOM;
            case "(Fach)":
                return OLD_SUBJECT;
            case "(Lehrer)":
                return OLD_TEACHER;
            case "(Klasse(n))":
                return OLD_CLASS;
            case "Vertretungs-Text":
            case "Vertr.Text":
            case "Text":
                return INFO;
            case "Vertr. von":
                // Apparently this represents a point in time at which the lesson was originally supposed to be held
                return OLD_TIME;
            default:
                return UNDEFINED;
        }
    }

    private boolean isUselessLine(String string) {
        String[] uselessLines = {"Abwesende Klassen", "Betroffene Klassen", "Abwesende Lehrer"};

        for (String useless :
                uselessLines) {
            if (string.contains(useless)) {
                // If it contains something useless, this line is useless
                return true;
            }
        }

        // It didn't contain anything useless and thus is not useless
        return false;
    }
}
