package godau.fynn.dsbdirect.table.reader;

import godau.fynn.dsbdirect.table.Entry;

import javax.annotation.Nullable;
import java.util.ArrayList;

public class Dummy extends Reader {
    public Dummy() {
        super(null, null);
    }

    @Override
    public ArrayList<Entry> read() {

        // For issue #33 https://notabug.org/fynngodau/DSBDirect/issues/33
        addEntry("10a · D", "3", null, "E47 · statt Mi", null);
        addEntry("10a · D", "4", null, "E47 · statt Mi", null);
        addEntry("10b · F 2. FS", "5", null, "U5 · Raumänderung", null);
        addEntry("10b · F 2. FS", "6", null, "U5 · Raumänderung", null);
        addEntry("10c", "5", null, "--- · fällt aus", null);
        addEntry("10d · M", "3", null, "E44 · statt Mi", null);

        return super.getEntries();
    }

    @Nullable
    @Override
    public String getSchoolName() {
        String[] names = {"Baumschule", "Dummydatengymnasium", "Max-Mustermann-Realschule"};

        int rand = (int) (Math.random() * names.length); // Cut off decimals

        return names[rand];
    }
}
