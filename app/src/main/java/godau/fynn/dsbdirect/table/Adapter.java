/*
 * DSBDirect
 * Copyright (C) 2019 Fynn Godau
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * This software is not affiliated with heinekingmedia GmbH, the
 * developer of the DSB platform.
 */

package godau.fynn.dsbdirect.table;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import godau.fynn.dsbdirect.R;
import godau.fynn.dsbdirect.Utility;
import org.sufficientlysecure.htmltextview.HtmlTextView;

import java.util.ArrayList;


public class Adapter extends ArrayAdapter<Entry> {

    private final Context mContext;
    private final ArrayList<Entry> mValues;

    public static final String LAYOUT_LIST = "list";
    public static final String LAYOUT_CARDS = "cards";
    public static final String LAYOUT_CARDS_REVERSED = "cards reversed";

    public Adapter(Context context, ArrayList<Entry> values) {
        super(context, R.layout.row_list,
                values // contents don't really matter
        );

        mContext = context;
        mValues = values;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        Entry entry = mValues.get(position);

        // which layout should be loaded?
        Utility u = new Utility(mContext);
        String layout = u.getSharedPreferences().getString("layout", LAYOUT_CARDS);

        // recycle stuff – I don't know how it works but it reduces lag
        View rowView;
        if (convertView != null) {
            rowView = convertView;
        } else {
            // load correct layout
            switch (layout) {
                case LAYOUT_LIST:
                    rowView = inflater.inflate(R.layout.row_list, parent, false);
                    break;
                case LAYOUT_CARDS_REVERSED:
                    rowView = inflater.inflate(R.layout.row_card_reversed, parent, false);
                    break;
                case LAYOUT_CARDS:
                default:
                    rowView = inflater.inflate(R.layout.row_card, parent, false);
                    break;
            }
        }


        // populate all fields
        fillField(entry.getAffectedClass(), (HtmlTextView) rowView.findViewById(R.id.affectedClass));
        fillField(entry.getLesson(), (HtmlTextView) rowView.findViewById(R.id.lesson));
        fillField(entry.getReplacementTeacher(), (HtmlTextView) rowView.findViewById(R.id.replacementTeacher));
        fillField(entry.getInfo(), (HtmlTextView) rowView.findViewById(R.id.info));

        // special code for cards
        if (layout.equals(LAYOUT_CARDS) || layout.equals(LAYOUT_CARDS_REVERSED)) {

            // remove divider
            ListView listView1 = (ListView) parent;
            listView1.setDivider(null);
            listView1.setDividerHeight(0);

            // color card according to status
            CardView cardView = rowView.findViewById(R.id.card);

            if (
                    entry.isHighlighted() ||
                            (entry.getAffectedClass().isEmpty() && entry.getLesson().isEmpty() && entry.getReplacementTeacher().isEmpty())) {
                // highlight card
                cardView.setCardBackgroundColor(u.getColorAccent());
            } else {
                // don't highlight card
                cardView.setCardBackgroundColor(u.getColorPrimary());
            }
        } else {
            // highlighting code for list

            int color = entry.isHighlighted() ? u.getColorAccent() : mContext.getResources().getColor(R.color.textOverWhite);

            ((HtmlTextView) rowView.findViewById(R.id.affectedClass)).setTextColor(color);
            ((HtmlTextView) rowView.findViewById(R.id.lesson)).setTextColor(color);
            ((HtmlTextView) rowView.findViewById(R.id.replacementTeacher)).setTextColor(color);
            ((HtmlTextView) rowView.findViewById(R.id.info)).setTextColor(color);
        }

        TextView dateView = rowView.findViewById(R.id.date);
        try {
            if (position != 0 && !mValues.get(position - 1).getDate().equals(entry.getDate())) {
                String date = u.formatDate(entry.getDate());
                dateView.setText(date);
                dateView.setVisibility(View.VISIBLE);
            } else {
                dateView.setVisibility(View.GONE);
            }
        } catch (NullPointerException e) {
            e.printStackTrace(); // Bad parser
        }


        return rowView;
    }

    private void fillField(String s, HtmlTextView v) {
        if (s != null && !s.isEmpty()) {
            v.setVisibility(View.VISIBLE);
            v.setHtml(s);
        } else {
            v.setVisibility(View.GONE);
        }
    }

    public void addAll(ArrayList<Entry> collection) {
        mValues.addAll(collection);
        notifyDataSetChanged();
    }

    public int size() {
        return mValues.size();
    }

    public Entry get(int index) {
        return mValues.get(index);
    }
}
