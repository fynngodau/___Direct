/*
 * DSBDirect
 * Copyright (C) 2019 Fynn Godau
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * This software is not affiliated with heinekingmedia GmbH, the
 * developer of the DSB platform.
 */

package godau.fynn.dsbdirect.table;

import android.support.annotation.Nullable;
import godau.fynn.dsbdirect.Utility;
import godau.fynn.dsbdirect.manager.ShortcodeManager;

import java.util.Date;

public class Entry {
    private String mAffectedClass;
    private String mLesson;
    private String mReplacementTeacher;
    private String mInfo;
    private Date mDate;
    private boolean mHightlighted = false;

    public Entry(@Nullable String affectedClass, @Nullable String lesson, @Nullable String replacementTeacher,
                 @Nullable String info, @Nullable Date date, @Nullable ShortcodeManager shortcodeManager) {
        mAffectedClass = affectedClass;
        mLesson = lesson;
        mReplacementTeacher = replacementTeacher;
        mInfo = info;
        mDate = date;

        if (mAffectedClass == null) {
            mAffectedClass = "";
        }
        if (mLesson == null) {
            mLesson = "";
        } else {
            mLesson = mLesson.replaceAll(" ", "");
        }
        if (mReplacementTeacher == null) {
            mReplacementTeacher = "";
        } else {
            // If not null, apply shortcodes
            if (shortcodeManager != null) {
                mReplacementTeacher = shortcodeManager.replace(mReplacementTeacher);
            }
        }
        if (mInfo == null) {
            mInfo = "";
        }
    }

    public String getAffectedClass() {
        return mAffectedClass;
    }

    public void setAffectedClass(String affectedClass) {
        mAffectedClass = affectedClass;
    }

    public String getLesson() {
        return mLesson;
    }

    public void setLesson(String lesson) {
        mLesson = lesson;
    }

    public String getReplacementTeacher() {
        return mReplacementTeacher;
    }

    public void setReplacementTeacher(String replacementTeacher) {
        mReplacementTeacher = replacementTeacher;
    }

    public String getInfo() {
        return mInfo;
    }

    public void setInfo(String info) {
        mInfo = info;
    }

    public Date getDate() {
        return mDate;
    }

    public void setDate(Date date) {
        mDate = date;
    }

    public String getShareText(Utility u) {
        String text = Utility.smartConcatenate(new String[]{
                u.formatDate(mDate),
                mAffectedClass,
                mLesson,
                mReplacementTeacher,
                mInfo
        }, " · ");

        // There might be some html in there that needs to go away
        text = text.replaceAll("<br>", "\n");
        text = text.replaceAll("</*strike>", "~");

        return text;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj.getClass() == Entry.class) {

            Entry entry = (Entry) obj;

            // Compare this entry to another entry
            return     entry.getAffectedClass().equals(getAffectedClass())
                    && entry.getLesson().equals(getLesson())
                    && entry.getReplacementTeacher().equals(getReplacementTeacher())
                    && entry.getInfo().equals(getInfo())
                    && entry.getDate().equals(getDate());

        } else {
            return super.equals(obj);
        }
    }

    public void setHighlighted() {
        mHightlighted = true;
    }

    public boolean isHighlighted() {
        return mHightlighted;
    }
}
