package godau.fynn.dsbdirect.activity.settings;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import godau.fynn.dsbdirect.Utility;

public abstract class SettingsActivity extends Activity {

    public static final String EXTRA_CONTAINS_HTML = "htmlTable";
    public static final String EXTRA_HTML_ONLY = "htmlOnlyTable";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        new Utility(SettingsActivity.this).stylize();

        // Display the fragment as the main content
        getFragmentManager().beginTransaction().replace(android.R.id.content,
                getPreferenceFragment()).commit();

    }

    @Override
    public void onBackPressed() {
        // Possibly trigger recreate
        setResult(RESULT_OK);

        super.onBackPressed();
    }

    abstract PreferenceFragment getPreferenceFragment();

    public static abstract class PreferenceFragment extends android.preference.PreferenceFragment {

        protected Utility u;
        protected SharedPreferences sharedPreferences;

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            // Load correct sharedPreferences
            getPreferenceManager().setSharedPreferencesName("default");

            u = new Utility(getActivity());
            sharedPreferences = u.getSharedPreferences();

        }
    }

}
